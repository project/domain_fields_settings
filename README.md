Domain Fields Settings
======================

INTRODUCTION
------------

Domain Fields Settings module for Drupal 8 community users.

* This module provide additional functionality to the admin user for accessing
  fields based on domain.
* The main functionality of this module to provide an admin interface from
  which we have to set particular fields based on the active domain for editing
  and / or adding node content.

CONFIGURATION
--------------
Domain Fields Settings module have configuration setting page
to set site Fields setting against each domains.

Configuration page path:
/admin/config/domain/domain_fields_settings

REQUIREMENTS
-------------
- domain
- domain_config

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.
