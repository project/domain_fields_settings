<?php

namespace Drupal\domain_fields_settings\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class DomainSiteSettingsController.
 *
 * @package Drupal\domain_fields_settings\Controller
 */
class DomainFieldsSettingsController extends ControllerBase {

  /**
   * The domain loader definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $domainLoader;

  /**
   * Construct function.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->domainLoader = $entityTypeManager->getStorage('domain');
  }

  /**
   * Create function return static domain loader configuration.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Load the ContainerInterface.
   *
   * @return \static
   *   return domain loader configuration.
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('entity_type.manager')
    );
  }

  /**
   * Function Provide the list of domain.
   *
   * @return typearray
   *   Domain list.
   */
  public function domainList() {
    $domains = $this->domainLoader->loadMultipleSorted();
    $rows = [];
    /** @var \Drupal\domain\DomainInterface $domain */
    foreach ($domains as $domain) {
      $row = [
        $domain->label(),
        $domain->getCanonical(),
        Link::fromTextAndUrl($this->t('Edit'), Url::fromRoute('domain_fields_settings.fields_config_form', ['domain_id' => $domain->id()])),
      ];
      $rows[] = $row;
    }
    // Build a render array which will be themed as a table.
    $build['pager_example'] = [
      '#rows' => $rows,
      '#header' => [
        $this->t('Name'),
        $this->t('Hostname'),
        $this->t('Edit Settings'),
      ],
      '#type' => 'table',
      '#empty' => $this->t('No domain record found.'),
    ];
    return $build;
  }

}
