<?php

namespace Drupal\domain_fields_settings\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Field\FieldFilteredMarkup;

/**
 * Class DomainFieldsConfigSettingsForm.
 *
 * @package Drupal\domain_fields_settings\Form
 */
class DomainFieldsConfigSettingsForm extends ConfigFormBase {

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * File storage for files.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestContext $request_context, EntityTypeManagerInterface $entityTypeManager, EntityFieldManager $entity_field_manager) {
    parent::__construct($config_factory);

    $this->requestContext = $request_context;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('config.factory'), $container->get('router.request_context'), $container->get('entity_type.manager'), $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'domain_fields_settings.domainfieldsconfigsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_config_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('domain_fields_settings.domainfieldsconfigsettings');
    $domain_id = $this->getRequest()->get('domain_id');
    $field_settings_data = [];
    $field_settings_data = $config->get($domain_id . '.field_setting');

    $domain_data = $this->entityTypeManager->getStorage('domain')->load($domain_id);

    $site_config = $this->config('system.site');
    $site_mail = $site_config->get('mail');
    if (empty($site_mail)) {
      $site_mail = ini_get('sendmail_from');
    }
    if ($config->get($domain_id) != NULL) {
      $site_mail = $config->get($domain_id . '.site_mail');
    }

    $types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    foreach ($types as $key => $value) {

      $fields = $this->entityFieldManager->getFieldDefinitions('node', $key);
      foreach ($fields as $field_name => $field_definition) {
        if (!empty($field_definition->getTargetBundle())) {
          $listFields[$field_name] = $field_definition->getLabel() . ' ( ' . $field_definition->getName() . ' )';
        }
      }
      unset($listFields['status']);
      unset($listFields['uid']);
      unset($listFields['created']);
      unset($listFields['changed']);
      unset($listFields['promote']);
      unset($listFields['sticky']);
      unset($listFields['metatag']);
      unset($listFields['path']);
      unset($listFields['publish_on']);
      unset($listFields['unpublish_on']);
      unset($listFields['menu_link']);
      $form['#tree'] = TRUE;
      $form['description'] = [
        '#markup' => FieldFilteredMarkup::create(
          '<p>' . $this->t('The following form allows you to alter the Content field settings for the domain %name (%domain).', ['%name' => $domain_data->label(), '%domain' => $domain_data->getCanonical()]) . '</p>
          <p>' . $this->t('Required, labels and help text should alter most fields.') . '</p>
          '
        ),
      ];
      $form['content_type_fieldset'][$key] = [
        '#type' => 'fieldset',
        '#title' => $value->label(),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      ];
      foreach ($listFields as $k => $value) {
        $form['content_type_fieldset'][$key][$k] = [
          '#type' => 'details',
          '#title' => $value,
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
        ];
        $form['content_type_fieldset'][$key][$k]['required'] = [
          '#title' => $this->t('Force requried'),
          '#type' => 'checkbox',
          '#default_value' => !empty($field_settings_data[$key][$k]['required']) ? $field_settings_data[$key][$k]['required'] : 0,

        ];
        $form['content_type_fieldset'][$key][$k]['remove'] = [
          '#title' => $this->t('Remove from forms'),
          '#type' => 'checkbox',
          '#default_value' => !empty($field_settings_data[$key][$k]['remove']) ? $field_settings_data[$key][$k]['remove'] : 0,

        ];
        $form['content_type_fieldset'][$key][$k]['label'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Label override'),
          '#default_value' => !empty($field_settings_data[$key][$k]['label']) ? $field_settings_data[$key][$k]['label'] : '',
        ];
        $form['content_type_fieldset'][$key][$k]['help'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Help override'),
          '#default_value' => !empty($field_settings_data[$key][$k]['help']) ? $field_settings_data[$key][$k]['help'] : '',
        ];
      }

      unset($listFields);
    }

    $form['domain_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Domain ID'),
      '#default_value' => $domain_id,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $domain_id = $form_state->getValue('domain_id');
    $formData = $form_state->getValue('content_type_fieldset');
    $data = [];
    foreach ($formData as $key => $value) {
      foreach ($value as $k => $v) {
        if (array_filter($v) && count($v) > 0) {
          $data[$key][$k] = $v;
        }
      }
    }
    $config = $this->config('domain_fields_settings.domainfieldsconfigsettings');

    if (count($data)) {
      $config->set($domain_id . '.field_setting', $data);
    }
    $config->save();
  }

}
